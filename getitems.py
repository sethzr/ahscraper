#!/usr/bin/env python3.6
'''Module to get the AH items'''
import asyncio
from itertools import groupby
from sys import argv
from deps.utils import read_cookie, print_gold
from lxml import html
from pandas import DataFrame, set_option
from numpy import where, nan_to_num
from deps.tabulate import tabulate
from aiohttp import ClientSession
from deps.items import item_list

global argN, arg1, arg2

# Handling args
# ./getitems                                : return all items from list
# ./getitems <min_ilvl>                     : return all items from list with min ilvl
# ./getitems <min_ilvl> <max_ilvl>          : return all items from list between min and max ilvl
# ./getitems <name>                         : return only specific item (partial match)
# ./getitems <name> <min_ilvl>
# ./getitems <name> <min_ilvl> <max_ilvl>

argN = len(argv)
if argN > 1:
    if not argv[1].isdigit():
        to_remove = []
        for item in item_list:
            if not argv[1].lower() in item[1].lower():
                to_remove.append(item)
        [item_list.remove(x) for x in to_remove]
        if argN > 2:
            arg1 = int(argv[2])
            arg2 = int(argv[3]) if argN > 3 else 999
        else:
            argN = 1
    else:
        arg1 = int(argv[1])
        arg2 = int(argv[2]) if argN > 2 else 999



def make_url(item):
    '''Build URL from item_id'''

    return ('https://eu.battle.net/wow/en/vault/character/auction/browse'
            '?sort=ilvl&start=0&end=200&itemId={}'.format(item[0]))


def parse_auctions(tree):
    '''HTML Parser to get the auctions'''

    #item_count = tree.xpath('//strong[@class="results-total"]/text()')[0]
    
    bid_path = '//tbody/tr/td[@class="price"]/div[@class="price-bid"]/span[@class="{}"]/text()''
    bo_patch = '//tbody/tr/td[@class="price"]/div[contains(@class,"price-buyout")]/span[@class="{}"]/text()'
    
    item_name = tree.xpath(
        ('//tbody/tr/'
         'td[@class="item"]'
         '/a/strong/text()'))

    #item_seller = tree.xpath('//tbody/tr/td[@class="item"]/a[contains(@href,"character")]/text()')

    item_ilvl = [t.strip() for t in tree.xpath(
        ('//tbody/tr'
         '/td[@class="level"]'
         '/text()[1]'))]

    item_socket = ["1808" in t for t in tree.xpath(
        ('//tbody/tr/'
         'td[@class="item"]'
         '/a[starts-with(@class,"icon-frame")]/@data-item'))]

    #item_time = tree.xpath('//tbody/tr/td[@class="time"]/span/text()')

    item_bid_g = tree.xpath(bid_path.format('icon-gold')

    item_bid_s = tree.xpath(bid_path.format('icon-silver')

    item_bid_c = tree.xpath(bid_path.format('icon-copper')

    item_bo_g = tree.xpath(bo_patch.format('icon-gold')

    item_bo_s = tree.xpath(bo_patch.format('icon-silver')

    item_bo_c = tree.xpath(bo_patch.format('icon-copper')



    item_bid = [(int(g.replace(',', '')) * 10000 + int(s) * 100 + int(c))
                for g, s, c in zip(item_bid_g, item_bid_s, item_bid_c)]
    item_bo = [(-1 if g == '--' and s == '--' and c == '--'
                else (int(g.replace(',', ''))* 10000 + int(s) * 100 + int(c)))
               for g, s, c in zip(item_bo_g, item_bo_s, item_bo_c)]

    return [([name, int(ilvl), socket, bid, bo])
            for name, ilvl, socket, bid, bo in
            zip(item_name, item_ilvl, item_socket, item_bid, item_bo)]


async def fetch_page(url, session):
    '''Make request, call parser'''

    async with session.get(url) as response:
        response = await response.read()
        data = response.decode('UTF-8')
        tree = html.fromstring(data)

        try:
            return parse_auctions(tree)
        except:
            print('Retry call')
            return []


async def get_auctions():
    '''Getting auctions from item list'''

    urls = map(make_url, item_list)

    cookies = read_cookie()
    async with ClientSession(cookies=cookies) as session:
        tasks = []
        for url in urls:
            task = asyncio.ensure_future(fetch_page(url, session))
            tasks.append(task)
        return [item for sublist in await asyncio.gather(*tasks) for item in sublist]


def arrange_data(auction_data):
    '''Pandas transformations, getting min values and grouping with sockets'''

    set_option('display.width', 1000)
    set_option('display.max_rows', 1000)

    df = DataFrame(auction_data)

    df.columns = ['Name', 'iLvl', 'Socket_', 'Bid_', 'BO_']
    df['Bid'] = df.groupby(['Name', 'iLvl'])['Bid_'].transform('min')
    df['Bo'] = df.groupby(['Name', 'iLvl'])['BO_'].transform('min')

    df['Socket__'] = where(df.Socket_, df.BO_, None)
    df['Socket'] = df.groupby(['Name', 'iLvl'])['Socket__'].transform('min')


    df = df.drop(['Bid_', 'BO_', 'Socket_', 'Socket__'], axis=1)
    df = df.drop_duplicates(['Name', 'iLvl', 'Socket'])

    df = df.melt(id_vars=['Name', 'iLvl'], value_vars=['Bid', 'Bo', 'Socket'])


    df.value = nan_to_num(df.value).astype(int)
    df.value = df.value.apply(lambda x: print_gold(x, True))

    df = df[df.value != 'k']
    if argN > 1:
        df = df[(df.iLvl >= arg1) & (df.iLvl <= arg2)]

    df.columns = ['Name', 'iLvl', '-', 'value']
    df = df.pivot_table(index=['Name', '-'], columns=['iLvl'], values='value', aggfunc='min')
    df = df.fillna(value='')

    return df



def fancy_format(df):
    '''Formatting the Dataframe for pretty print'''

    fancy_index = []
    c_type = 0
    auction_type = {
        0: ' Bid',
        1: '  BO'
    }
    for _, g in groupby(df.index.labels[0]):
        items = list(g)
        fancy_index.append(df.index.levels[0][items[0]].ljust(26) +
                           auction_type.get(df.index.labels[1][c_type], '  [+]'))
        c_type += 1
        if len(items) > 1:
            for i in range(1, len(items)):
                fancy_index.append('                           ' +
                                   auction_type.get(df.index.labels[1][c_type], '  [+]'))
                c_type += 1
                i += 1
    return tabulate(df, showindex=fancy_index, headers='keys', tablefmt='rst', stralign='right')



loop = asyncio.get_event_loop()
future = asyncio.ensure_future(get_auctions())

auctions = loop.run_until_complete(future)

try:
    dataframe = arrange_data(auctions)
    print(fancy_format(dataframe))
except:
    print('Nothing was received. Check cookies maybe')
