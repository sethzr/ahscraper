"""Various utility methods for AH item getting"""
#from pycookiecheat2 import chrome_cookies
from sys import path

def write_cookie(url):
    """Writes chrome cookie information to file"""

    cookies = []
    #
    #cookies = chrome_cookies(url)
    #       try:
    file = open(path[0] + '/deps/cookies.bnet', 'w')
    for cookie in cookies:
        file.write(cookie)
        file.write(':')
        file.write(cookies[cookie])
        file.write('\n')
    file.close()


def read_cookie():
    """Reads cookie info from file"""

    cookie_file = open(path[0] + '/deps/cookies.bnet', 'r')
    cookie_list = dict()

    for line in cookie_file:
        line = line.rstrip()
        if line[-1] == '\n':
            line = line[:-1]
        (name, value) = line.split(':')
        cookie_list[name] = value
    return cookie_list


def print_gold(gold, simple):
    """Prints gold in a more readable way"""
    gold_str = str(gold)
    if simple:
        if gold == -1:
            return '-'
        return gold_str[:-7] + 'k'
        #return gold_str[:-7] + ' ' + gold_str[-7:-4]
    return gold_str[:-7] + ' ' + gold_str[-7:-4] + ' ' + gold_str[-4:-2] + ' ' + gold_str[-2:]
